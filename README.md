# MagicBook
MagicBook is a PWA that allows users to build decks, battle, and communicate with friends.  It contains a feed similar to what facebook uses in order to let friends know when you win a match or create a new deck.  Since MTG is such a  complicated game, the gaming mechanism depends on the users being honest and be their own referee, much like an in person game.  

MagicBook is currently implemented using the following technologies.  If you would like to contribute, it would be beneficial for you to learn the basics of these frameworks.

    - Vue.js
    - vuex
    - firebase
    - bootstrap vue <- planned to be replaced with vuetify

## Working on an issue
Issues can be found under the Issues->List on Gitlab.  When you've decided to work on an issue, select to assign it to yourself and move it into the Doing section under Issues->Board.  A branch can be setup by clicking on the drop down arrow on the "Create merge request" button and selecting "Create branch".  The default naming convention should be used for the name of the branch created.

## Getting setup
MagicBook depends on npm version 6.4.1.  Reference the npm project for instructions for installing the needed version.

For development, I recommend using VS Code.  The project can be ran in a terminal within VS Code and give the same development feeling as using a full IDE ide for compiled languages, checked in using an included sc manager, has a number of convenient plugins.

### Installing dependencies
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn run build
```

### Lints and fixes files
```
yarn run lint
```
