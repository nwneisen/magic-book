import Vue from 'vue'
import Vuex from 'vuex'
import user from './modules/user'
import deck from './modules/deck'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    user,
    deck
  },
  state: { // = data
  },
  getters: { // = computed properties
  },
  mutations: { // = setting and updating state
  },
  actions: { // = methods
  },
})