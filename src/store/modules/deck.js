// import firebase from 'firebase'
import {
  db
} from '@/main'

export default ({
  state: { // = data
    deck: {
      name: "",
      description: "",
      user: "",
      cards: []
    }
  },
  getters: { // = computed properties
    getDeck: function (state) {
      return state.deck;
    },
  },
  mutations: { // = setting and updating state
    addToDeck: function (state, cardName) {
      const mtg = require("mtgsdk");
      mtg.card.where({
        name: cardName
      }).then(cards => {
        state.deck.cards.push({
          name: cards[0].name,
          imageUrl: cards[0].imageUrl,
          id: cards[0].id,
          manaCost: cards[0].manaCost,
          types: cards[0].types
        });
        // eslint-disable-next-line
        console.log(cards);
      });
    },
    setDeck: function (state, deck) {
      state.deck = deck;
    },
    resetDeck: function (state) {
      state.deck.cards = [];
    },
    replaceDeck: function (state, newDeck) {
      state.deck.cards = [];

      newDeck.list.cards.forEach(card => {
        state.commit("addToDeck", card.name);
      });
    },
    saveDeck: function (state, deckName) {
      state.deck.name = deckName;

      db.collection(state.deck.name).add({
        deck: state.deck
      });

      alert("Deck saved?")
    }
  },
  actions: { // = methods
  },
})