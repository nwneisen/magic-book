import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

Vue.config.productionTip = false

// Vuetify includes
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import 'material-design-icons-iconfont/dist/material-design-icons.css'
Vue.use(Vuetify)

import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
Vue.use(BootstrapVue)

// VueStore includes
import VueFire from 'vuefire'
import firebase from 'firebase/app'
import 'firebase/firestore'
Vue.use(VueFire)
firebase.initializeApp({
  apiKey: "AIzaSyCqo7mjOTD4Z1EFb-lnkrnccPc4_CaViXI",
  authDomain: "magicbook-19dee.firebaseapp.com",
  databaseURL: "https://magicbook-19dee.firebaseio.com",
  projectId: "magicbook-19dee",
  storageBucket: "magicbook-19dee.appspot.com",
  messagingSenderId: "583835712793"
})
export const db = firebase.firestore()
const firestore = firebase.firestore()
const settings = {
  timestampsInSnapshots: true
};
firestore.settings(settings);

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')