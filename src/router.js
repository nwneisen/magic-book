import Vue from 'vue'
import Router from 'vue-router'
import Feed from '@/views/Feed.vue'
import Game from '@/views/Game.vue'
import Login from '@/views/Login.vue'
import Register from '@/views/Register.vue'
import Builder from '@/views/Builder.vue'
import Logout from '@/views/Logout.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [{
      path: '/',
      name: 'feed',
      component: Feed
    },
    {
      path: '/game',
      name: 'game',
      component: Game
    }, {
      path: '/builder',
      name: 'builder',
      component: Builder
    },
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    {
      path: '/register',
      name: 'register',
      component: Register
    },
    {
      path: '/logout',
      name: 'logout',
      component: Logout,
    },
  ]
})